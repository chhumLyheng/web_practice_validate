import { Component } from "react";

export default class Validate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      person: [
        {
          name: "Lyheng",
          age: "22",
        },
      ],
      newName: "",
      newAge: "",
      ms_name: "",
      ms_num: "",
      ms: "",
      ms_suc: "",
    };
  }

// Name Validatin
  handleName = (e) => {
    if (e.target.value.match(/^[a-zA-Z]*$/)) {
      this.setState({
        ms_name: "",
        newName: e.target.value,
      });
    } else {
      this.setState({
        ms_name: "You Have to Input only charactor!!!",
      });
    }
  };

  // Age validation
  handleAge = (e) => {
    let numberRegex = /^\d+$/;
    if (e.target.value.match(numberRegex)) {
      this.setState({
        ms_num: "",
        newAge: e.target.value,
      });
    } else {
      this.setState({
        ms_num: "You Have to Input Ur Age!!! ",
      });
    }
  };

  // Button Submit Validation
  submit = (e) => {
    e.preventDefault();
    if (this.state.newName != "" && this.state.newAge != "") {
      const newObj = {
        name: this.state.newName,
        age: this.state.newAge,
      };
      this.setState(
        {
          ms_suc: "Display Table",
          ms: "",
          person: [...this.state.person, newObj],
        },
        () => console.log("New", this.state.person)
      );
    } else {
      this.setState({
        ms_suc: "",
        ms: "It's Errors... Please Input Ur informations to continue.",
      });
    }
  };

  render() {
    return (
      <div>
        <form>

          {/* Input Name */}
          <label>Name:</label>
          <input onChange={this.handleName} type="text"></input>
          <br />
          <label>{this.state.ms_name}</label>
          <br />

        {/* Input Age */}
          <label>Age:</label>
          <input onChange={this.handleAge} type="text"></input>
          <br />
          <label>{this.state.ms_num}</label>
          <br />

      {/* Button Submit */}
          <button onClick={this.submit}>Submit</button>
          <p>{this.state.ms}</p>
          <p>{this.state.ms_suc}</p>
        </form>

        {/* Table Display */}
        <center>
          <table border="1">
            <thead>
              <tr>
                <th>Name</th>
                <th>Age</th>
              </tr>
            </thead>
            <tbody>
              {this.state.person.map((m) => (
                <tr>
                  <td>{m.name}</td>
                  <td>{m.age}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </center>


      </div>
    )
  }
}
